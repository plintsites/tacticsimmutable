let helpers =  {

  	calculateScore: function(score) {
    	let scoreObj = score.toJS();
    	return Object.keys(scoreObj).reduce(function(previousValue, currentValue) {
		    return previousValue + Math.max(0, scoreObj[currentValue].hits - 3) * currentValue;
    	}, 0);
	},

	/**
	 * Given this.state.data, return the active player or the other player specified by type
	 * @param  {Immutable Map} data
	 * @param  {string}        type
	 * @return {Immutable Map}
	 */
	getPlayer: function(data, type) {
		return data.get(this.getPlayerKey(data, type));
	},

	/**
	 * Given this.state.data, return the key, i.e., 'player1' or 'player2' based on type
	 * @param  {Immutable Map} data
	 * @param  {string}        type
	 * @return {Immutable Map}
	 */
	getPlayerKey: function(data, type) {
		if (type === 'active') {
			return 'player' + data.get('gameTurn');
		} else if (type === 'other') {
			return 'player' + (3 - data.get('gameTurn'));
		}
	},

	/**
	 * check if a game is finished (called from hitNumber).
	 * @param  {Immutable Map} data
	 * @param  {int}           number [number hit by active player]
	 * @param  {Immutable Map} activePlayer
	 * @return {bool}
	 */
	gameFinished: function(data, number, activePlayer) {
		// Note that current player is also finished when for number hits == 2 (completed after this hit)
		var finished = true;
		for (var i = 10; i <= 20; i++) {
			if (i === number) {
				// Make sure other player has 'number' finished, this is saved in activePlayer!
				finished = finished && activePlayer.getIn(['score', i.toString(), 'completed']);
				// Active player is finished after this hit, i.e., at the moment its number of hits on the number == 2
				finished = finished && activePlayer.getIn(['score', i.toString(), 'hits']) === 2;
			} else {
				finished = finished && data.getIn(['player1', 'score', i.toString(), 'completed']);
				finished = finished && data.getIn(['player2', 'score', i.toString(), 'completed']);
			}
		}

		return finished;
	},

	getCleanScore: function() {
		return {
			'20': {hits: 0, completed: false},
			'19': {hits: 0, completed: false},
			'18': {hits: 0, completed: false},
			'17': {hits: 0, completed: false},
			'16': {hits: 0, completed: false},
			'15': {hits: 0, completed: false},
			'14': {hits: 0, completed: false},
			'13': {hits: 0, completed: false},
			'12': {hits: 0, completed: false},
			'11': {hits: 0, completed: false},
			'10': {hits: 0, completed: false}
		};
	}

}

export default helpers;
